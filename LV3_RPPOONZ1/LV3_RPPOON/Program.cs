﻿using System;
using System.Collections.Generic;

namespace LV3_RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset data = new Dataset("C:\\Users\\dejan\\source\\repos\\LV3_RPPOON\\LV3 - List 1.csv");
            foreach (List<string> list in data.GetData())
            {
                foreach (string str in list)
                    Console.WriteLine(str);
            }
            data.Clone();
            foreach (List<string> list in data.GetData())
            {
                foreach (string str in list)
                    Console.WriteLine(str);
            }
        }
    }
}
/*U ovom zadatku nije bilo potrebe za dubokim kopiranjem jer se ne predaje
  neka druga klasa nego se samo stvaraju kopije članova ove klase*/


