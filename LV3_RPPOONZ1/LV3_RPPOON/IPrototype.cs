﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3_RPPOON
{
    interface IPrototype
    {
        IPrototype Clone();
    }
}
