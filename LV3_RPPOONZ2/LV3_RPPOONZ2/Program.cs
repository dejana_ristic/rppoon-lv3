﻿using System;

namespace LV3_RPPOONZ2
{
    class Program
    {
        static void Main(string[] args)
        {
            double[][] matrica = new double[3][];
            for (int i = 0; i < 3; i++)
            {
                matrica[i] = new double[3];
            }
            MatrixGenerator matrixGenerator = MatrixGenerator.GetInstance();
            matrica = matrixGenerator.NextMatrix(3, 3);
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Console.Write(matrica[i][j].ToString() + '\t');
                }
                Console.WriteLine();
            }
        }
    }
}
