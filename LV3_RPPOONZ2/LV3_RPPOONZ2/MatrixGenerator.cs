﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3_RPPOONZ2
{
    class MatrixGenerator
    {
        private static MatrixGenerator instance;
        private Random generator;
        private MatrixGenerator()
        {
            this.generator = new Random();
        }
        public static MatrixGenerator GetInstance()
        {
            if (instance == null)
                instance = new MatrixGenerator();
            return instance;
        }
        public double[][] NextMatrix(int rows, int collumns)
        {
            double[][] matrix = new double[rows][];
            for (int i = 0; i < rows; i++)
            {
                matrix[i] = new double[collumns];
            }
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    matrix[i][j] = this.generator.NextDouble();
                }
            }
            return matrix;
        }
/* Ova metoda inicijalizira matricu i podstavlja njene vrijednosti*/
    }
}
