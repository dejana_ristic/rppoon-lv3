﻿using System;

namespace LV3_RPPOONZ4
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime time = new DateTime();
            Category level = new Category();
            ConsoleColor color = new ConsoleColor();
            color = ConsoleColor.Red;
            level = Category.ALERT;
            time = DateTime.Now;
            ConsoleNotification consoleNotification = new ConsoleNotification
                ("Me", "DO NOT FORGET", "Do your homework!", time, level, color);
            NotificationManager notification = new NotificationManager();
            notification.Display(consoleNotification);

        }
    }
}
