﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3_RPPOONZ5
{
    class NotificationBuilder: IBuilder
    {
        public String Author { get; private set; }
        public String Title { get; private set; }
        public String Text { get; private set; }
        public DateTime Timestamp { get; private set; }
        public Category Level { get; private set; }
        public ConsoleColor Color { get; private set; }
        public IBuilder SetAuthor(string author)
        {
            this.Author = author;
            return this;
        }

        public IBuilder SetColor(ConsoleColor color)
        {
            this.Color = color;
            return this;
        }

        public IBuilder SetLevel(Category level)
        {
            this.Level = level;
            return this;
        }

        public IBuilder SetText(string text)
        {
            this.Text = text;
            return this;
        }

        public IBuilder SetTime(DateTime time)
        {
            this.Timestamp = time;
            return this;
        }

        public IBuilder SetTitle(string title)
        {
            this.Title = title;
            return this;
        }
        public NotificationBuilder()
        {
            this.Author = "NN";
            this.Title = "Title:";
            this.Text = "Note:";
            this.Timestamp = DateTime.Now;
            this.Level = Category.ERROR;
            this.Color = ConsoleColor.Black;
        }
        public ConsoleNotification Build()
        {
            return new ConsoleNotification
                (this.Author, this.Title, this.Text, this.Timestamp, this.Level, this.Color);
        }
    }
}
