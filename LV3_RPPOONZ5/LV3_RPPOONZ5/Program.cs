﻿using System;

namespace LV3_RPPOONZ5
{
    class Program
    {
        static void Main(string[] args)
        {
            NotificationManager Director = new NotificationManager();
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            ConsoleNotification consoleNotification = notificationBuilder.Build();
            Director.Display(consoleNotification);
            notificationBuilder.SetAuthor("Me");
            notificationBuilder.SetText("Do your homework");
            notificationBuilder.SetColor(ConsoleColor.Black);
            notificationBuilder.SetLevel(Category.ALERT);
            consoleNotification = notificationBuilder.Build();
            Director.Display(consoleNotification);

        }
    }
}
